<?php 
	require_once("config.php");
	require_once("Models/Pendiente.php");
	class ControllerPendientes {
		private $conexion;
		/*INSTRUCCIONES SQL*/
		private $SQL_ALL_PENDIENTES = "SELECT * FROM PENDIENTES";
		/*TERMINA SQL*/
		function conectar($HOST,$USERNAME,$PASSWORD,$DATABASE_NAME) {
			$this->conexion = mysqli_connect($HOST,$USERNAME,$PASSWORD,$DATABASE_NAME);
		}

		function getAllPendientes() {
			$res = mysqli_query($this->conexion,$this->SQL_ALL_PENDIENTES);
			$i = 0;
			
			while($fila = $res->fetch_row() ) {
				$p = new Pendiente(
						$fila[0],
						$fila[1],
						$fila[2],
						$fila[3],
						$fila[4],
						$fila[5],
                        $fila[6],
						$fila[7],
						$fila[8], 
                        $fila[9]
					);
				$arr[$i] = array(
					"id"=>$p->getId(),
					"fecha"=>$p->getfecha(),
					"hInicio"=>$p->getHInicio(),
					"hFin"=>$p->getHFin(),
					"prioridad"=>$p->getPrioridad(),
					"cliente_id"=>$p->getCliente_id(), 
                    "descripcion"=>$p->getDescripcion(),
                    "tipo"=>$p->getTipo(),
                    "titulo"=>$p->getTitulo(), 
                    "estado"=>$p->getEstado()
					);
				$i++;
			}
			return json_encode($arr);
		}
        
        function getPendientesByDay($d,$m,$a) {
            
		    $filter = " WHERE FECHA='$a-$m-$d'";
			$res = mysqli_query($this->conexion,$this->SQL_ALL_PENDIENTES.$filter);
			$i = 0;
			$arr = array();
			while($fila = $res->fetch_row() ) {
				$p = new Pendiente(
						$fila[0],
						$fila[1],
						$fila[2],
						$fila[3],
						$fila[4],
						$fila[5],
                        $fila[6],
						$fila[7],
						$fila[8], 
                        $fila[9]
					);
				$arr[$i] = array(
					"id"=>$p->getId(),
					"fecha"=>$p->getfecha(),
					"hInicio"=>$p->getHInicio(),
					"hFin"=>$p->getHFin(),
					"prioridad"=>$p->getPrioridad(),
					"cliente_id"=>$p->getCliente_id(), 
                    "descripcion"=>$p->getDescripcion(),
                    "tipo"=>$p->getTipo(),
                    "titulo"=>$p->getTitulo(), 
                    "estado"=>$p->getEstado() 
					);
				$i++;
			}
			return json_encode($arr);
		}


	}

?>