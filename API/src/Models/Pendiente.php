<?php 
	class Pendiente {
        private $id;
        private $fecha;
		private $hora_inicio;
		private $hora_fin;
		private $prioridad;
		private $cliente_id;
        private $descripcion;
        private $tipo;
        private $titulo;
        private $estado; 
        
		function __construct($id,$fecha,$hora_inicio,$hora_fin,$prioridad,$cliente_id,$descripcion,$tipo,$titulo,$estado) {

            $this->id = $id;
            
            $this->fecha = $fecha;

			$this->hora_inicio = $hora_inicio;

			$this->hora_fin = $hora_fin;

			$this->prioridad = $prioridad;

			$this->cliente_id = $cliente_id;

			$this->descripcion = $descripcion;  
            
            $this->tipo = $tipo;

			$this->titulo = $titulo;  
            
            $this->estado = $estado;  
		}
        
        function setId($id) {
			$this->id = $id;
		}

		function setFecha($fecha) {
			$this->fecha = $fecha;
		}

		function setHInicio($hora_inicio) {
			$this->hora_inicio = $hora_inicio;
		}
        
        function setHFin($hora_fin) {
			$this->hora_fin = $hora_fin;
		}

		function setPrioridad($prioridad) {
			$this->prioridad = $prioridad;
		}

		function setCliente_id($cliente_id) {
			$this->cliente_id = $cliente_id;
		}

		function setDescripcion($descripcion) {
			$this->descripcion = $descripcion;
		}

		function setTipo($tipo) {
			$this->tipo = $tipo;
		} 
        
        function setTitulo($titulo) {
			$this->titulo = $titulo;
		} 
        
        function setEstado($estado) {
			$this->estado = $estado;
		} 
        
        function getId() { return $this->id; }
		function getfecha() { return $this->fecha; }
		function getHInicio() { return $this->hora_inicio; }
		function getHFin() { return $this->hora_fin; }
		function getPrioridad() { return $this->prioridad; }
		function getCliente_id() { return $this->cliente_id; }
		function getDescripcion() { return $this->descripcion; }
        function getTipo() { return $this->tipo; }
		function getTitulo() { return $this->titulo; }
        function getEstado() { return $this->estado; }
		function __toString() {
			return $this->id.", ".$this->fecha.", ".$this->fecha_inicio.", ".$this->fecha_inicio.", ".$this->prioridad.", ".$this->cliente_id.", ".$this->descripcion.", ".$this->tipo.", ".$this->titulo.", ".$this->estado;
		}
	}
	
?>