<?php 
	class Cliente {
		private $id;
		private $nombre;
		private $direccion;
		private $telefono;
		private $clase;
		private $email;
		function __construct($id,$nombre,$direccion,$telefono,$clase,$email) {
			$this->id = $id;

			$this->nombre = $nombre;

			$this->direccion = $direccion;

			$this->telefono = $telefono;

			$this->clase = $clase;

			$this->email = $email;
		}

		function setId($id) {
			$this->id = $id;
		}

		function setNombre($nombre) {
			$this->nombre = $nombre;
		}

		function setDireccion($direccion) {
			$this->direccion = $direccion;
		}

		function setTelefono($telefono) {
			$this->telefono = $telefono;
		}

		function setClase($clase) {
			$this->clase = $clase;
		}

		function setEmail($email) {
			$this->email = $email;
		}
		function getId() { return $this->id; }
		function getNombre() { return $this->nombre; }
		function getDireccion() { return $this->direccion; }
		function getTelefono() { return $this->telefono; }
		function getClase() { return $this->clase; }
		function getEmail() { return $this->email; }
		function __toString() {
			return $this->id.", ".$this->nombre.", ".$this->direccion.", ".$this->telefono.", ".$this->clase.", ".$this->email;
		}
	}
	
?>