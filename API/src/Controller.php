<?php 
	require_once("config.php");
	require_once("Models/Cliente.php");
	class Controller {
		private $conexion;
		/*INSTRUCCIONES SQL*/

		private $SQL_ALL_CLIEENTES = "SELECT * FROM CLIENTES";
		/*TERMINA SQL*/
		function conectar($HOST,$USERNAME,$PASSWORD,$DATABASE_NAME) {
			$this->conexion = mysqli_connect($HOST,$USERNAME,$PASSWORD,$DATABASE_NAME);
		}

		function getAllClientes() {
			$res = mysqli_query($this->conexion,$this->SQL_ALL_CLIEENTES);
			$i = 0;
			$arr = array();
			while($fila = $res->fetch_row() ) {
				$c = new Cliente(
						$fila[0],
						$fila[1],
						$fila[2],
						$fila[3],
						$fila[4],
						$fila[5]
					);
				$arr[$i] = array(
					"id"=>$c->getId(),
					"nombre"=>$c->getNombre(),
					"direccion"=>$c->getDireccion(),
					"telefono"=>$c->getTelefono(),
					"clase"=>$c->getClase(),
					"email"=>$c->getEmail()
					);
				$i++;
			}
			return json_encode($arr);
		}


	}

?>
