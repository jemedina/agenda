function agregarPendiente() {
    var titulo = $("#titulo").val();
    var tipos = $("#tipos_pendientes").val();
    var prioridad = $("#prioridad_eventos").val();
    var fecha = $("#fecha").val();
    var h_inicio = $("#hora_inicio").val(); 
    var h_fin = $("#hora_fin").val();
    var descripcion = $("#descripcion").val();
    var idCliente = $("#cliente").val();    

    if(!colisionExists()) {
        $.ajax({
            url:"http://"+host+"/agendaapi/Data/nuevoPendiente.php",
            method:"POST",
            data:{
                "titulo":titulo,
                "tipos":tipos, 
                "prio":prioridad, 
                "fecha":fecha, 
                "hInicio":h_inicio,
                "hFin":h_fin,
                "descrip":descripcion,
                "idCliente":idCliente
            },
            success:function(data){
                alert("Respuesta del server: " + data);
            },
            error:function(err){
                alert("Error: "+err);
            }
        });
    } else {
        $("#popup_colision_info").css({"visibility":"visible","opacity": "1"});
    }
}

function agregarPendientePopUp() {
    var fecha = $("#fechaPop").val();
    var h_inicio = $("#init").val(); 
    var h_fin = $("#final").val(); 
    var id = $("#idPop").val();
    if(!colisionExistsNoMore()) {
        $.ajax({
            url:"http://"+host+"/agendaapi/Data/reagendar_pendiente.php",
            method:"POST",
            data:{
                "id":id,
                "fecha":fecha, 
                "hInicio":h_inicio,
                "hFin":h_fin,
            },
            success:function(data){
                alert("Respuesta del server: " + data);
            },
            error:function(err){
                alert("Error: "+err);
            }
        });
    } else {
        $("#popup_nomore").css({"visibility":"visible","opacity": "1"});
    }
}

function colisionExists() {
    var fecha = $("#fecha").val();
    var h_inicio = $("#hora_inicio").val(); 
    var h_fin = $("#hora_fin").val();
    var result = false;
    $.ajax({
        url:"http://"+host+"/agendaapi/Data/checarColision.php",
        method:"POST",
        async: false,
        data:{
            "fecha":fecha, 
            "hInicio":h_inicio,
            "hFin":h_fin,
        },
        success:function(data){
            alert(data);
            var colision = JSON.parse(data);
           if(data != "none"){
                $("#starttime").html(colision["hora_inicio"]);
                $("#endtime").html(colision["hora_fin"]);
                $("#datetime").html(colision["fecha"]);
                $("#citatitulo").html(colision["titulo"]);
                if(colision["prioridad"] == 1){
                    $("#citaprioridad").html("Baja");
                } else if(colision["prioridad"] == 2) {
                    $("#citaprioridad").html("Media");
                } else {
                    $("#citaprioridad").html("Alta");
                }
                $("#citacliente").html(colision["id_cliente"]);
                $("#idPop").val(colision["id"]);
                result = true;
            }
        },
        error:function(err){
            alert("Error: "+err);
            return false;
        }
    });
    return result;
}



function colisionExistsNoMore() {
    var fecha = $("#fechaPop").val();
    var h_inicio = $("#init").val(); 
    var h_fin = $("#final").val(); 
    var result = false;
    $.ajax({
        url:"http://"+host+"/agendaapi/Data/checarColision.php",
        method:"POST",
        async: false,
        data:{
            "fecha":fecha, 
            "hInicio":h_inicio,
            "hFin":h_fin,
        },
        success:function(data){
            alert(data);
            var colision = JSON.parse(data);
           if(data != "none"){
                $("#citatituloNoMore").html(colision["titulo"]);
                $("#citaclienteNoMore").html(colision["id_cliente"]);
                result = true;
            }
        },
        error:function(err){
            alert("Error: "+err);
            return false;
        }
    });
    return result;
}

function none(data) {
    console.log(data);
}