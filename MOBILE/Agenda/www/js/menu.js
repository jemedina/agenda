//Registrar el evento:
$(document).ready(function() {
	$("#buttonHeader").click(showOrHideMenu);
});

var status = -1;

function showOrHideMenu() {
	$menu = $("#menu");
	if(status == 1) {
		$menu.animate({
			"margin-left":"-100%"
		},100);
		status = -status;
	} else {
		$menu.animate({
			"margin-left":"0%"
		},100);
		status = -status;
	}
}